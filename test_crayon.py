import svgwrite
import math
import random
import os

DIMX = 800
DIMY = 600
svg = svgwrite.Drawing("test_papiercrayon.svg", size=(DIMX, DIMY))


nbptss = [10, 9, 8, 7, 6, 5, 4]
nbptss = [100, 100, 100, 100, 100, 100, 100]
radii = [300, 250, 200, 100, 50, 25, 10]
colors = [
    (100, 100, 220),
    (200, 100, 220),
    (200, 100, 100),
    (100, 200, 220),
    (100, 200, 100),
    (0, 100, 220),
    (220, 100, 220),
]


paper_filter = svgwrite.filters.Filter(
    id="paper", start=("-20%", "-20%"), size=("120%", "120%"), filterUnits="userSpaceOnUse"
)
pen_filter = svgwrite.filters.Filter(
    id="pen", start=("-20%", "-20%"), size=("120%", "120%"), filterUnits="userSpaceOnUse"
)

paper_filter.feTurbulence(baseFrequency=0.60399999999999998, numOctaves=1, result="turbulence", coucou="salut")
paper_filter.feColorMatrix(in_="turbulence", type="saturate", values="0", result="gray")
paper_filter.feColorMatrix(
    in_="gray", type="matrix", values="1 0 0 0 -0.4 0 1 0 0 -0.4 0 0 1 0 -0.4 0 0 0 0.2 0", result="low_opacity"
)

paper_filter.feDiffuseLighting(
    in_="low_opacity",
    surfaceScale=2.101,
    diffuseConstant=1.1899999999999999,
    lighting_color="white",
    result="enlightened",
).feDistantLight(azimuth=50.078740157480318, elevation=52)
paper_filter.feGaussianBlur(in_="enlightened", stdDeviation=0.55000000000000004, result="blurred")
paper_filter.feComposite(in_="blurred", in2="SourceGraphic", operator="atop", result="composed")
paper_filter.feBlend(in_="SourceGraphic", in2="composed", mode="darken", result="blended")

pen_filter.feTurbulence(type="fractalNoise", numOctaves=7, baseFrequency=0.02, seed=55, result="turbulence")
pen_filter.feDiffuseLighting(
    in_="turbulence", surfaceScale=3.567, diffuseConstant=1, kernelUnitLength=1.073574144486692, result="enlightened"
).feDistantLight(azimuth=235, elevation=60)
pen_filter.feSpecularLighting(
    in_="turbulence",
    surfaceScale=1.22,
    specularConstant=1,
    specularExponent=25,
    kernelUnitLength=1.1,
    result="enlightened2",
).feDistantLight(azimuth=235, elevation=55)
pen_filter.feComposite(in_="enlightened", in2="SourceGraphic", operator="arithmetic", k1=1, result="composite")
pen_filter.feComposite(in_="composite", in2="enlightened2", operator="arithmetic", k2=1, k3=1, result="composite2")
pen_filter.feComposite(in2="SourceAlpha", in_="composite2", operator="in", result="composite3")
pen_filter.feDisplacementMap(in_="composite3", in2="turbulence", scale=6, yChannelSelector="G", xChannelSelector="R")

svg.defs.add(paper_filter)
svg.defs.add(pen_filter)

svg.add(svg.rect((0, 0), (DIMX, DIMY), fill="white", filter="url(#paper)"))
for i in range(len(radii)):
    nbpts = nbptss[i]
    radius = radii[i]

    fill_color = "none"  # svgwrite.rgb(0, 0, 0)
    dark = 100
    stroke_color = (dark, dark, dark)
    width = 3
    pl = svg.polyline(
        fill=fill_color,
        stroke=svgwrite.rgb(*stroke_color),
        stroke_width=width,
        stroke_linecap="round",
        stroke_linejoin="round",
        stroke_opacity=0.82,
        filter="url(#pen)",
    )

    for j in range(nbpts):
        pl.points.append(
            (
                400 + radius * math.cos(j / (nbpts - 1) * 2 * math.pi),
                300 + radius * math.sin(j / (nbpts - 1) * 2 * math.pi),
            )
        )

    for j in range(nbpts):
        pl.points.append((10 + j * (800 - 10) / nbpts, 20 + i * 85 + int(random.random() * 50)))

    svg.add(pl)


svg.save()
os.system("inkscape -z test_papiercrayon.svg -e test_papiercrayon.png")
