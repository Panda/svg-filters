import svgwrite
import math
import random
import os

DIMX = 800
DIMY = 600
svg = svgwrite.Drawing("test_neon.svg", size=(DIMX, DIMY))


def add_polyline(svg, polyline, color, width):
    nstep = 10
    twidth = 2 * width
    widthstep = (twidth - width) / nstep
    dark = 0.7
    darker = svgwrite.rgb(int(color[0] * dark), int(color[1] * dark), int(color[2] * dark))
    for i in range(nstep):
        pl = svg.polyline(
            fill="none",
            stroke=darker,
            stroke_width=twidth,
            opacity=1 / nstep,
            stroke_linecap="round",
            stroke_linejoin="round",
        )
        pl.points = polyline.points
        svg.add(pl)
        twidth -= widthstep
    pl = svg.polyline(
        fill="none", stroke=darker, stroke_width=width, opacity=1, stroke_linecap="round", stroke_linejoin="round",
    )
    pl.points = polyline.points
    svg.add(pl)
    polyline.attribs["stroke-width"] = 0.5 * width
    svg.add(polyline)


def add_filtered_polyline(svg, polyline, color, width):
    pl = svg.polyline(
        fill="none",
        stroke=svgwrite.rgb(*color),
        stroke_width=5.1 * width,
        stroke_linecap="round",
        stroke_linejoin="round",
        filter="url(#filter817)",
    )
    pl.points = polyline.points
    svg.add(pl)
    svg.add(polyline)


nbptss = [10, 9, 8, 7, 6, 5, 4]
nbptss = [100, 100, 100, 100, 100, 100, 100]
radii = [300, 250, 200, 100, 50, 25, 10]
colors = [
    (100, 100, 220),
    (200, 100, 220),
    (200, 100, 100),
    (100, 200, 220),
    (100, 200, 100),
    (0, 100, 220),
    (220, 100, 220),
]
svg.add(svg.rect((0, 0), (DIMX, DIMY), fill="black"))

filter817 = svgwrite.filters.Filter(
    id="filter817", start=("-20%", "-20%"), size=("120%", "120%"), filterUnits="userSpaceOnUse"
)
dark = 0.5
filter817.feColorMatrix(
    in_="SourceGraphic",
    type_="matrix",
    values=f"{dark} 0 0 0 0 0 {dark} 0 0 0 0 0 {dark} 0 0 0 0 0 1 0",
    result="darkened"
    # values=[dark, 0, 0, 0, 0, 0, dark, 0, 0, 0, 0, 0, dark, 0, 0, 0, 0, 0, 1, 0],
)
filter817.feGaussianBlur(in_="darkened", stdDeviation=12)

svg.defs.add(filter817)

for i in range(len(radii)):
    nbpts = nbptss[i]
    radius = radii[i]

    fill_color = "none"  # svgwrite.rgb(0, 0, 0)
    stroke_color = colors[i]
    width = 5
    pl = svg.polyline(
        fill=fill_color,
        stroke=svgwrite.rgb(*stroke_color),
        stroke_width=width,
        stroke_linecap="round",
        stroke_linejoin="round",
    )

    for j in range(nbpts):
        pl.points.append(
            (
                400 + radius * math.cos(j / (nbpts - 1) * 2 * math.pi),
                300 + radius * math.sin(j / (nbpts - 1) * 2 * math.pi),
            )
        )

    for j in range(nbpts):
        pl.points.append((10 + j * (800 - 10) / nbpts, 20 + i * 85 + int(random.random() * 50)))

    add_filtered_polyline(svg, pl, stroke_color, width)


svg.save()
name = "test_neon"
# os.system("inkscape -z test_neon.svg -e test_neon.png")
os.system(f"rsvg-convert {name}.svg > {name}.png")
